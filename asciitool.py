# -*- Mode: Python; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import sys
import itertools
import hashlib
import argparse
import shelve

from colorama import init, Style, Fore, Back

from scanner import Scanner
from comment import ASCII_RE, Comment
from utility import overwrite_range, check_negative, edit_vim, get_indent, trim_newlines


DEFAULT_NUM_CONTEXT_LINES = 5
TERMINAL_ROWS_HALF = int(os.popen('stty size', 'r').read().split()[0]) / 2


class Reasons:
    ASCII = 1
    USELESS = 2
    BAD_DOXYGEN = 3


def analyze_comment(comment, args):
    is_doxygen = comment.is_doxygen_formatted()
    if args.ignore_doxygen and is_doxygen:
        return None, None

    # First, check for comments with a useless first line
    new_content = comment.cleaned()
    seen, cleaned_content = comment.content_without_useless_first_line()
    if seen:
        # Replace content with the cleaned content
        new_content = cleaned_content
        if not args.no_useless:
            return new_content, Reasons.USELESS

    if not args.no_ascii and has_ascii_art(comment):
        reason = Reasons.ASCII
    elif not args.no_bad_doxygen and not is_doxygen and len(comment.lines) > 1 and comment.has_doxygen_commands():
        # Check for Doxygen comments that aren't formatted correctly
        # TODO: Technically C++ style comments need to be at least one line to be considered by Doxygen;
        # should check if that's actually true in practice
        reason = Reasons.BAD_DOXYGEN
    else:
        return None, None

    return new_content, reason


def display_commands():
    print Fore.MAGENTA + Style.BRIGHT + "y - take best guess"
    print "n - do nothing"
    print "N - do nothing, and ignore in the future"
    print "d - delete entire block"
    print "f - do nothing, and finish up with this file"
    print "q - quit"
    print "v - edit best guess"
    print "V - edit original" + Fore.RESET + Style.RESET_ALL


def display_lines(before, content, after, start_line, color):
    context_before = [Style.DIM + "%4d: " % (start_line + i + 1) + Fore.RESET + line + Style.RESET_ALL for i, line in
                      enumerate(before)]
    offset = start_line + len(before)
    match_lines = [color + Style.DIM + "%4d: " % (offset + i + 1) + Style.BRIGHT + line + Style.RESET_ALL + Fore.RESET
                   for i, line in enumerate(content)]
    offset = start_line + len(content) + len(before)
    context_after = [Style.DIM + "%4d: " % (offset + i + 1) + Fore.RESET + line + Style.RESET_ALL for i, line in
                     enumerate(after)]
    print "\n".join(itertools.chain(context_before, match_lines, context_after)) + "\n"


def has_ascii_art(comment):
    # Ignore license text or Vim stuff at the start of the file
    if comment.start <= 10 and "license" in comment.raw_content.lower() or "tab-width: 4;" in comment.raw_content:
        return False

    if ASCII_RE.search(comment.raw_content):
        return True

    return False


def process_file(path, ignores, args):
    ignores_out = []
    ignores_out.extend(ignores)
    ignore_hashes = [i[0] for i in ignores]
    with open(path, "r+") as file_obj:
        contents = file_obj.read()
        if not contents:
            return False, [], False

        has_trailing_newline = contents[-1] == '\n'
        lines = contents.splitlines()

        # Extract comments
        comments = Scanner.extract_comments(contents)
        ranges = [(comment.start, comment.end) for comment in comments]

        pending_saves = 0
        rescan_needed = False
        for comment in comments:
            if rescan_needed:
                break

            this_hash = hashlib.md5(comment.raw_content).hexdigest()
            if this_hash in ignore_hashes:
                print "Ignoring %s in %s" % (this_hash, path)
                continue

            # Do we need to process the comment?
            comment._fill_context(lines, args.context)
            new_content, reason = analyze_comment(comment, args)
            if not reason:
                continue

            # If the comment is going to take a lot of space, then we should reset the terminal
            # so the user doesn't have a hard time scrolling around
            if not args.no_reset and (len(comment.lines) + args.context * 2) >= TERMINAL_ROWS_HALF:
                os.system("reset")
            else:
                os.system("clear")

            # Check whether the context lines for this comment intersect the next match
            ranges.pop(0)
            check_next_match = ranges and (
                comment.before_start <= ranges[0][0] < comment.start or comment.before_start <= ranges[0][
                    1] < comment.start)

            # Display the comment as it currently is
            file_name = path + (" (%d changes pending save)" % pending_saves if pending_saves else "")
            print Style.BRIGHT + "Original (%s):" % file_name + Style.RESET_ALL
            display_lines(comment.before, comment.lines, comment.after, comment.before_start, Fore.RED)

            # Reformat the content with the cleaned up content
            new_content = trim_newlines(new_content)

            as_doxygen = (reason == Reasons.BAD_DOXYGEN) or comment.is_doxygen_formatted()
            new_content = Comment.format_comment(new_content, get_indent(comment.lines), comment.is_c_style,
                                                 doxygen=as_doxygen,
                                                 leading_star=not args.no_leading_star)

            # Display reformatted comment
            print Style.BRIGHT + "Best guess:" + Style.RESET_ALL
            display_lines(comment.before, new_content, comment.after, comment.before_start, Fore.GREEN)

            # Read command
            command = None
            valid_commands = ['y', 'n', 'N', 'd', 'f', 'q', 'v', 'V']
            while not command in valid_commands:
                sys.stdout.write(
                    Fore.YELLOW + Style.BRIGHT + "Choice [%s,?] ? " % ",".join(
                        valid_commands) + Fore.RESET + Style.RESET_ALL)

                command = sys.stdin.readline()[0:2]
                if command[0] == '?':
                    display_commands()
                    command = None
                elif pending_saves and command[0] == "q" and command[1] != "!":
                    print Fore.YELLOW + Back.RED + Style.BRIGHT + "Unsaved changes. Use q! to override." + Style.RESET_ALL
                    command = None
                command = command[0] if command else None

            # Process command
            if command == 'd':
                comment.before, _, comment.after = collapse_newlines(comment.before, [], comment.after)
                overwrite_range(comment.before_start, comment.after_end, lines, comment.before + comment.after)
            elif command == 'y':
                overwrite_range(comment.before_start, comment.after_end, lines,
                                comment.before + new_content + comment.after)
            elif command == 'q':
                return False, ignores_out, True
            elif command == 'n' or command == 'N':
                # tuple structure: (hash, whether to ignore in future)
                ignores_out.append((hashlib.md5(comment.raw_content).hexdigest(), command == 'N'))
                continue
            elif command == 'f':
                break
            elif command == 'v' or command == 'V':
                result = edit_vim(comment.before + (comment.lines if command == 'V' else new_content) + comment.after)
                overwrite_range(comment.before_start, comment.after_end, lines, result)

                # Check whether we modified the next match
                if check_next_match:
                    for i, line in enumerate(comment.before):
                        if line != result[i] and ranges[0][0] <= comment.before_start + i <= ranges[0][1]:
                            rescan_needed = True
                            pending_saves += 1
                            break

            pending_saves += 1

        if pending_saves:
            file_obj.seek(0)
            file_obj.write("\n".join(lines) + ('\n' if has_trailing_newline else ''))
            file_obj.truncate()
            file_obj.flush()

    return rescan_needed, ignores_out, False


def main():
    arg_parser = argparse.ArgumentParser(description="Interactively detect and clean badly formatted comments")
    arg_parser.add_argument("-a", "--no-ascii", action="store_true",
                            help="Don't explicitly take comments with ASCII art")
    arg_parser.add_argument("-b", "--no-bad-doxygen", action="store_true",
                            help="Don't explicitly take incorrectly formatted Doxygen comments")
    arg_parser.add_argument("-d", "--ignore-doxygen", action="store_true",
                            help="Ignore all Doxygen comments (implies -b)")
    arg_parser.add_argument("-l", "--no-leading-star", action="store_true",
                            help="Don't add a leading * to C-style comment lines")
    arg_parser.add_argument("-n", dest="context", type=check_negative, default=DEFAULT_NUM_CONTEXT_LINES,
                            help="Number of context lines to display before and after each comment")
    arg_parser.add_argument("-p", dest="ignores_file", default="ignores.persist",
                            help="Path to the persistence file used to remember comment ignore decisions")
    arg_parser.add_argument("-r", "--no-reset", action="store_true",
                            help="Don't reset the terminal if a long comment is going to be displayed")
    arg_parser.add_argument("-u", "--no-useless", action="store_true",
                            help="Don't explicitly take comments with useless first lines")

    args = arg_parser.parse_args()

    # -d implies -b
    if args.ignore_doxygen:
        args.no_bad_doxygen = True

    init()  # for Colorama

    extensions = [".cxx", ".hxx", ".h", ".m", ".mm"]
    store = shelve.open(args.ignores_file)

    should_quit = False
    for root, _, files in os.walk("."):
        if should_quit:
            break

        for file_name in files:
            if should_quit:
                break
            if not any([file_name.endswith(extension) for extension in extensions]):
                continue

            path = os.path.join(root, file_name)
            path_hash = hashlib.md5(os.path.abspath(path)).hexdigest()

            rescan = True
            ignores = [(i, True) for i in store[path_hash]] if path_hash in store else []
            while rescan:
                rescan, ignores, should_quit = process_file(path, ignores, args)

            # Filter ignores so we are left only with ones that should be persisted
            ignores = [i[0] for i in ignores if i[1]]
            if ignores:
                store[path_hash] = ignores

    store.close()


if __name__ == "__main__":
    main()
# -*- Mode: Python; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import re

from utility import normalize_whitespace, trim_newlines


ASCII_RE = re.compile("[-\*+=#<>]{4,}")

METHOD_DECL_RE = re.compile(r"^[\w:\s]*?([~\w]+)\s*\(.*")
NAMESPACE_RE = re.compile(r"^\s*(\w+)\s*::.*")
TYPE_DECL_RE = re.compile(r"^\s*(?:template[\w\s<>]+)?\s*(?:class|namespace|struct|enum)\s+(\w+)")

KEYWORD_RE = re.compile(
    "(class(es)?|constructor[s]?|declaration[s]?|define[s]|destructor[s]?|enum[s]?|export(s|ed)|extern"
    "[s]?|factor(y|ies)|forward|friend[s]?|function[s]?|implementation[s]?|inline[s]?|interface[s]?|"
    "internal[s]?|method[s]?|namespace[s]?|operator[s]?|private|propert(y|ies)|protected|public|static|"
    "struct[s]?|typedef[s]?|virtual)")

LEADING_CLEANER = re.compile(r"^\s*[-|\*+=#/\\]+")
TRAILING_CLEANER = re.compile(r"(^|\s)[-|\*+=#/\\]+$")
DOXYGEN_C_STYLE = re.compile(r"/\*[\*!](?!\*)")
DOXYGEN_CPP_STYLE = re.compile(r"^\s*//[/!].*")

# Most of the commands that Doxygen supports
DOXYGEN_COMMAND_RE = re.compile(
    r"[@\\](addindex|addtogroup|anchor|arg|attention|author|authors|brief|bug|callgraph|callergraph|category|cite|"
    r"class|code|cond|copybrief|copydetails|copydoc|copyright|date|def|defgroup|deprecated|details|diafile|dir|"
    r"docbookonly|dontinclude|dot|dotfile|else|elseif|em|endcode|endcond|enddocbookonly|enddot|endhtmlonly|endif"
    r"|endinternal|endlatexonly|endlink|endmanonly|endmsc|endparblock|endrtfonly|endsecreflist|endverbatim|"
    r"endxmlonly|enum|example|exception|extends|file|fn|headerfile|hideinitializer|htmlinclude|"
    r"htmlonly|idlexcept|if|ifnot|image|implements|include|includelineno|ingroup|internal|invariant|interface|"
    r"latexinclude|latexonly|li|line|link|mainpage|manonly|memberof|msc|mscfile|name|namespace|nosubgrouping|"
    r"note|overload|package|page|par|paragraph|param|parblock|post|pre|private|privatesection|property|protected|"
    r"protectedsection|protocol|public|publicsection|pure|ref|refitem|related|relates|relatedalso|relatesalso|"
    r"remark|remarks|result|return|returns|retval|rtfonly|sa|secreflist|section|see|short|showinitializer|since|"
    r"skip|skipline|snippet|struct|subpage|subsection|subsubsection|tableofcontents|test|throw|throws|tparam"
    r"|typedef|union|until|var|verbatim|verbinclude|version|vhdlflow|warning|weakgroup|xmlonly|xrefitem)")


def extract_definitions_from_context(context):
    for line in context:
        match = METHOD_DECL_RE.match(line)
        if match:
            # Grab the namespace while we're at it
            namespace_match = NAMESPACE_RE.match(line)
            if namespace_match:
                yield namespace_match.group(1) + "::" + match.group(1)

            yield match.group(1)

        match = TYPE_DECL_RE.match(line)
        if match:
            yield match.group(1)


class Comment:
    def __init__(self, lines, start, end, is_c_style):
        self.lines = lines[start:end + 1]
        self.start = start
        self.end = end
        self.is_c_style = is_c_style
        self.before = self.after = self.before_start = self.after_end = None
        self.left_align = self.content_lines = None
        self._cleaned = None

    @staticmethod
    def format_c_style(content, indent, doxygen, leading_star):
        ret = []
        got_content = False
        for line in content:
            if line.strip() or got_content:
                if not got_content:
                    ret.append(indent + ("/**" if doxygen else "/*"))
                ret.append(indent + (" * " if leading_star else "    ") + line)
                got_content = True
            else:
                ret.append("")

        return ret + [indent + " */"]

    @staticmethod
    def format_cpp_style(content, indent, doxygen):
        ret = []
        got_content = False
        for line in content:
            if line.strip() or got_content:
                ret.append(indent + ("/// " if doxygen else "// ") + line)
                got_content = True
            else:
                ret.append("")
        return ret
    @staticmethod
    def format_comment(content, indent, c_style, doxygen=False, leading_star=True):
        """
        force_style: True to force C-style, False to force C++ style.
        """
        content = trim_newlines(content)
        if not content:
            return []

        if len(content) < 5:
            c_style = False

        indent = " " * indent
        # Force C-style commenting for Doxygen comments of one line
        if c_style or (doxygen and len(content) == 1):
            return Comment.format_c_style(content, indent, doxygen, leading_star)

        return Comment.format_cpp_style(content, indent, doxygen)

    def content_without_useless_first_line(self):
        """
        Returns a tuple containing:
            - Boolean indicating whether a useless line was removed.
            - The cleaned content lines.
        """
        cleaned = []
        seen_useless = False
        definitions = extract_definitions_from_context(self.after)
        for line in self.cleaned():
            if not line:
                cleaned.append(line)
            elif not seen_useless and KEYWORD_RE.sub("", line).translate(None, " ()<>!.,;") in definitions:
                seen_useless = True
                continue
            else:
                cleaned.append(line)

        return seen_useless, normalize_whitespace(cleaned)

    def cleaned(self):
        """
        Returns the comment lines stripped of ASCII art and leading/trailing garbage characters, such as leading *
        for C style comments.
        """
        # Clean ASCII art (and cache the result)
        if self._cleaned is None:
            self._cleaned = []
            for line in normalize_whitespace([ASCII_RE.sub("", line) for line in self.lines]):
                # Trim leading garbage
                text = LEADING_CLEANER.sub("", line)
                if text.strip().startswith("Beschreibung:"):
                    text = text.strip()[len("Beschreibung:"):]
                # Trim trailing garbage
                self._cleaned.append(TRAILING_CLEANER.sub("", text))
            self._cleaned = normalize_whitespace(self._cleaned)
        return self._cleaned

    def has_doxygen_commands(self):
        """
        Determines whether any line of the comment contains a Doxygen command.
        """
        return any([DOXYGEN_COMMAND_RE.search(line) for line in self.cleaned()])

    def is_doxygen_formatted(self):
        """
        Determines whether a comment is formatted as a Doxygen comment.
        """
        # Check for the comment types given in http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html
        # Test for /// or //! style comments (needs to be at least 2 lines)
        if len(self.lines) > 1 and all([not line.strip() or DOXYGEN_CPP_STYLE.match(line) for line in self.lines]):
            return True

        # Test for /** style comments
        i = 0
        while not self.lines[i].strip():
            i += 1

        if DOXYGEN_C_STYLE.search(self.lines[i]):
            return True

        return False

    def _fill_context(self, lines, num):
        # Calculate number of context lines
        self.before_start = self.start - min(num, self.start)
        self.after_end = self.end + min(num, len(lines) - self.end - 1)

        # Extract lines
        self.before = lines[self.before_start:self.start]
        self.after = lines[self.end + 1:self.after_end + 1]

    @property
    def raw_content(self):
        return '\n'.join(self.lines)
# -*- Mode: Python; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import tempfile
from subprocess import call


# http://stackoverflow.com/a/14117511/221061
def check_negative(value):
    ivalue = int(value)
    if ivalue < 0:
        raise argparse.ArgumentTypeError("# of context lines must be >= 0")
    return ivalue


def edit_vim(lines):
    with tempfile.NamedTemporaryFile(suffix=".tmp") as tmp:
        tmp.write("\n".join(lines))
        tmp.flush()
        call(["vim", tmp.name])
        tmp.file.seek(0)
        return tmp.read().splitlines()


def get_indent(lines):
    for line in lines:
        if line.strip():
            return len(line) - len(line.lstrip())


def normalize_whitespace(lines):
    ret = []
    left_align = -1
    for line in lines:
        if left_align < 0 and line.strip():
            left_align = len(line) - len(line.lstrip())

        i = 0
        while i < left_align and i < len(line) and line[i] == ' ':
            i += 1

        ret.append(line[i:])
    return ret


def overwrite_range(start, end, target, source):
    for i in range(end, start - 1, -1):
        target.pop(i)

    for line in reversed(source):
        target.insert(start, line.rstrip())


def trim_newlines(lines, reverse=False, allow_leading=True):
    ret = []
    seen_content = False
    if reverse:
        lines = reversed(list(lines))
    for line in lines:
        temp = line.strip()
        if temp or seen_content or (not ret and allow_leading):
            ret.append(line)
        if temp:
            seen_content = True
    return list(reversed(ret)) if reverse else ret


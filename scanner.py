# -*- Mode: Python; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import re
import itertools
import operator

from comment import Comment


# Matches /* */ style comments
C_STYLE = re.compile("""^[ \t]*/\*.+?\*/""", re.DOTALL | re.MULTILINE)
# Matches // style comments
CPP_STYLE = re.compile("""^([ \t]*//[^\n\r]*)(\n[ \t]*//[^\n\r]*)*""", re.MULTILINE)


class Scanner:
    @staticmethod
    def extract_comments(contents):
        matches = []
        lines = contents.splitlines()

        # Search for for comment blocks and // style comments
        for match in itertools.chain(C_STYLE.finditer(contents), CPP_STYLE.finditer(contents)):
            start = contents.count("\n", 0, match.start()) + 1

            # We might have come up short
            if start > 0 and lines[start - 1] == match.group(0).splitlines()[0]:
                start -= 1

            end = contents.count("\n", 0, match.end())
            if start > end:
                start = end

            # Consume adjacent newlines
            while (start > 1) and not lines[start - 1].strip():
                start -= 1

            while (end < len(lines) - 1) and not lines[end + 1].strip():
                end += 1

            matches.append(
                Comment(lines, start, end, match.re == C_STYLE))

        return sorted(matches, key=operator.attrgetter("start"), reverse=True)